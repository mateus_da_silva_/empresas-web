class Auth {
  constructor() {
    this.token = ""
  }

  login = async body => {
    const response = await fetch('https://empresas.ioasys.com.br/api/v1/users/auth/sign_in', {
      method: "POST",
      headers:{"Content-Type": "application/json"},
      body: JSON.stringify(body)
    })

    for (let pair of response.headers.entries()) {
      if(pair[0] === 'access-token'){
        localStorage.setItem(pair[0],pair[1]);
      }
      if(pair[0] === 'client')
        localStorage.setItem(pair[0],pair[1]);
      if(pair[0] === 'uid')
        localStorage.setItem(pair[0],pair[1]);
    }
    return response;
  }

  isLoged() {
    return this.token = localStorage.getItem("access-token")
  }
  
}

export default new Auth();