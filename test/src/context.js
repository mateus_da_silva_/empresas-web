import React, {createContext, useState, useContext} from 'react';

const DetailsContext = createContext();

export default function DetailsProvider({children}) {
  const [company, setCompany] = useState({});

  return (
    <DetailsContext.Provider value ={
      company, setCompany
    }>
      {children}
    </DetailsContext.Provider>
  )
}

export const useDetails = () => {
  const context = useContext(DetailsContext)
  const {company, setCompany} = context
  return {company, setCompany};
}