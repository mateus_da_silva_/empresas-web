import React from 'react';
import {Login} from './components/login';
import {BrowserRouter as Router,
  Switch,
  Route,
  Redirect} from 'react-router-dom';
import DetailsProvider from './context'
import {Home} from './components/home';
import {CompanyDetails} from './components/details'
import auth from './auth';

function App() {
  return (
    <DetailsProvider>
      <Router>
        <Switch>
          <Route exact path="/"><Login /></Route>
          {auth.isLoged() !== "" ? <Route
          exact patch="/home"><Home /></Route>: <Redirect to="/" />}
          {auth.isLoged() !== "" ? <Route
          exact patch="/details"><CompanyDetails /></Route>: <Redirect to="/" />}
        </Switch>
      </Router>
    </DetailsProvider>
  )
}

export default App;
