
export const get = async (url) => {

  const response = await fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "access-token": localStorage.getItem("access-token"),
      "client": localStorage.getItem("client"),
      "uid": localStorage.getItem("uid")
    }
  })
  return response.json()
}