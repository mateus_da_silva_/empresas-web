import React, {useState, useEffect} from 'react';
import style from './home.module.css'
import {ListCompanies} from '../listCompanies'
import {get} from '../../get.routes'

export const Home = () => {
  const [search, setSearch] = useState("")
  const [enterprises, setEnterprises] = useState([]);
  useEffect(() => {
    if(search !== ""){
    get(`https://empresas.ioasys.com.br/api/v1/enterprises?enterprise_types=1&name=${search}`)
      .then(response => setEnterprises(response.enterprises))
      .catch(err => console.error(err.message))
    }
  }, [search])
  const mouseDown = () => {
    if(search === "") {
      get("https://empresas.ioasys.com.br/api/v1/enterprises")
      .then(response => setEnterprises(response.enterprises))
      .catch(err => console.error(err.message))
    }
  }
  const onSearch = e => {
    setSearch(e.target.value);
  }
  const List = () => {
    if(enterprises.length === 0 && search === ""){
      return <p className="col s12 valign-wrapper">Clique na busca para começar</p>
    }
    else if (enterprises.length === 0) {
      return <p className="col s12 valign-wrapper">Nenhuma empresa foi encontrada para a busca realizada</p>
    }
    else {
      return <ListCompanies enterprises={enterprises} />  
    }
  }
  return (
  <div className={`row ${style.background}`}>
    <div className={`col s12 ${style.navbar}`} >
        <img className="col s3 offset-m4" src="/logo-nav.png" alt=""/>
        <div className="input-field">
          <input type="search" 
          id="search" 
          onMouseDown={mouseDown} 
          value={search} 
          onChange={onSearch} />
          <label htmlFor="search" className="white-text" >Pesquisar</label>
        </div>
    </div>
    <div className="row">
      <List />
    </div>
  </div>
  )
}