import React, {useState, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import style from './login.module.css';
import auth from '../../auth';

export const Login = props => {
  let history = useHistory();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const signIn = e => {
    
    e.preventDefault();
    const body = {
      email: email,
      password: password
    };
    auth.login(body);
    history.push("/home")
  }
  return (
    <div className={`row center-align ${style.background}`} >
      <img src="/logo-home.png" alt="logo" className={style.logo} />
      <div className="container col s12">
        <p>BEM VINDO AO EMPRESAS</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        <form onSubmit={signIn}>
        <div className="row">
          <div className="input-field col s4 offset-s4">
              <input placeholder="Placeholder" 
              id="e-mail" 
              type="text" 
              className="validate" 
              value={email} 
              onChange={e => setEmail(e.target.value)} />
              <label htmlFor="first_name">E-mail</label>
          </div>
        </div>
        <div className="row">
          <div className="input-field col s4 offset-s4">
              <input placeholder="Placeholder" 
              id="password" 
              type="password" 
              className="validate" 
              value={password}
              onChange={e => setPassword(e.target.value)}
              />
              <label htmlFor="first_name">Senha</label>
              <button className={`btn-large col s4 ${style.button}`}>Entrar</button>
          </div>
        </div>
        </form>
      </div>
    </div>
  )
}
