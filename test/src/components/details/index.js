import React from 'react';
import {useDetails} from '../../context';
import style from './details.module.css';

export const CompanyDetails = () => {
  const {company} = useDetails();
  return (
    <div className="row">
      <div className={`col s12 ${style.navbar}`} >
        <p>{company.enterprise_name}</p>        
      </div>
      <p>{company.description}</p>
    </div>
  )
}