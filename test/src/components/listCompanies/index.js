import React, {useState} from 'react';
import {useHistory} from 'react-router-dom'
import style from './list-companies.module.css';
import {get} from '../../get.routes';
import {useDetails} from '../../context'

export const ListCompanies = props => {
  const [company, setCompany] = useDetails;
  const history = useHistory
  const requestDetails = id => {
    get(`https://empresas.ioasys.com.br/api/v1/enterprises/${id}`)
    .then(response => {
      setCompany(response);
      history.push("/details")
    })
    .catch(err => err.message)
  }

  return (
    <div className="container row">
      {props.enterprises && props.enterprises.map(company => (
      <div className={`col s12 card ${style.companyCard}`} key={`company${company.id}`}
      onClick={ () => requestDetails(company.id)}
      >
        <img src={`https://empresas.ioasys.com.br/api/v1/enterprises${company.photo}`} alt="copany photo"/>
        <div className={style.cardText}>
          <p>{company.enterprise_name}</p>
          <p>{company.enterprise_type.enterprise_type_name}</p>
          <p>{company.country}</p>
        </div>
      </div>
      ))}
    </div>
  )
}
